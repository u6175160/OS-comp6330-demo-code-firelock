
/* hello world module - Eric McCreath 2005,2006,2008,2010,2012,2016 */
/* to compile use:
    make -C  /usr/src/linux-headers-`uname -r` SUBDIRS=$PWD modules
   to install into the kernel use :
    insmod hello.ko
   to test :
    cat /proc/hello
   to remove :
    rmmod hello
*/
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/module.h>
#include <linux/uaccess.h>
#include<linux/spinlock.h>
#include<linux/mutex.h>
//static char a[100];
static char* a=NULL;
static int hello_proc_show(struct seq_file *m, void *v)
{
    printk("proc hello show\n");
    printk("Q1test\n");
    seq_printf(m, "%s",a);
    return 0;
}
static int len=0;
static int hello_proc_write(struct file *m,const char __user *v, size_t t, loff_t *vv)
{
	int i=0;
	char* buffer = (char*)v;
	memset(a,0,100);
	printk("before%s",buffer);
	while(buffer[i]!='\0')
		i++;
	len = copy_from_user(a,v,t);
	printk("after%s",a);
	printk("write%d\n",len);
	return t;
}
static  bool flag=false;
static spinlock_t loc;
static int hello_proc_open(struct inode *inode, struct file *file)
{
//    if(flag==true)
//	return -ENOMEM;
//    flag=true;
    spin_lock(&loc);
    printk("true\n");
    return single_open(file, hello_proc_show, NULL);
}
static int hello_proc_release(struct inode *inode, struct file *file)
{
    flag=false;
    spin_unlock(&loc);
    printk("false\n");
    return single_release(inode,file);
}
static const struct file_operations hello_proc_fops = {
	.open		= hello_proc_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.write		= hello_proc_write,
	.release	= hello_proc_release,
};

static int __init proc_hello_init(void)
{
    spin_lock_init(&loc);
    printk("test");
 
    a = (char*)kmalloc(100,GFP_KERNEL);
    printk("init proc hello\n");
    proc_create("spin_lock", 00777, NULL, &hello_proc_fops);
    return 0;
}
static void __exit cleanup_hello_module(void)
{
    kfree(a);
	a=NULL;
    printk("cleanup proc hello\n");
    remove_proc_entry("spin_lock",NULL);
}


module_init(proc_hello_init);
module_exit(cleanup_hello_module);


